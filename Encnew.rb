include Raspberry
include Raspberry::Core
include Raspberry::Timing
include Raspberry::Specifics


pinA=0  #GPIO0 è pin 11   # Assegno il numero di pin per rendere la struttura facilmente modificabile
pinB=1  #GPIO1 è pin 12

Core::pin_mode(pinA,INPUT)     # A
Core::pin_mode(pinB,INPUT)     # B

Core::pull_mode(pinA,PUD_UP)   # Setto le resistenze come PULL UP (perchè: descritto su Word)
Core::pull_mode(pinB,PUD_UP) 

####################LOGICA########################
# =>  SEQUENZA    A   B   A^B   VALUE (4A +2B+A^B)
# =>  0           0   0   0     0      
# =>  1           1   0   1     3     
# =>  2           1   1   0     5     
# =>  3           0   1   1     4     
####################################################
#Da 0-->3-->5-->4 Giro ORARIO
#Da 4-->5-->3-->0 Giro ANTIORARIO

# Assegnazione costanti
new_value=0       #nuovo valore calcolato 
old_value=1       #vecchio valore calcolato: è 1 per permettere, solo al ciclo di avvio, di uguagliare new_value con old_value.                 
cont=0
tot=0
db=0

loop do
  
  # Leggo i pin: Restituisce 1(alto) oppure 0(basso)
  A=Core::digital_read(pinA)
  B=Core::digital_read(pinB)
  
  xor=(A^B)

  #Sono al 1° avvio: sicomme l'encoder è fermo, per solo il caso in cui old_value=1  definito nelle costanti
  # • Metto old_value = new_value
  # • Mi esegue il codice solo 1 volta
  # • Prosegue e non mi fa i confronti, così restituisce un valore 0: sono fermo.

  if old_value==1 then
    old_value = ( A*2 + B*3 + xor*1 ) #mi cambia subito il valore da 1 a (0,3,5 or 4) dunque verrà eseguito SOLO all'avvio.
  else 
    #db+=0
  end

  new_value = ( A*2 + B*3 + xor*1 )
  
  if new_value!=old_value then
  
    if new_value==0 then
      if old_value==4 then
        #puts "1 giro ORArio" 
        cont=1
      elsif old_value==3
        #then puts "1 giro ANTIorario"
        cont=(-1)    
      else
        #db+=0
      end
    end
  
    if new_value==3 then
      if old_value==0 then
        #puts "1 giro ORArio" 
        cont=1
      elsif old_value==5
        then #puts "1 giro ANTIorario"
        cont=(-1)
      else
        #db+=0
      end
    end
  
    if new_value==5 then
      if old_value==3 then
        #puts "1 giro ORArio"
        cont=1 
      elsif old_value==4
        then #puts "1 giro ANTIorario"
        cont=(-1)
      else
        #db+=0
      end
    end
  
    if new_value==4 then
      if old_value==5 then
        #puts "1 giro ORArio" 
        cont=1
      elsif old_value==0
        then #puts "1 giro ANTIorario"
        cont=(-1)
      else
        #db+=0
      end
    end
  
  else
    cont=0
  end

  tot+=cont  
  old_value = new_value 

  if tot==4 then
    puts "1 GIRO a { DX }"
    tot=0
  elsif tot==(-4) then
    puts "1 GIRO a { SX }"
    tot=0
  end

end