#################################################################################
#      Questo sketch permette la lettura di un encoder ottico incrementale
#      dotato di 2 canali A,B.
#      Alessandro Grillo, 12/10/2015 (È il mio compleanno!)
#################################################################################


#Setto i pin di input

include Raspberry
include Raspberry::Core
include Raspberry::Timing
include Raspberry::Specifics

 
pinA=0  #GPIO0 è pin 11   # Assegno il numero di pin per rendere la struttura facilmente modificabile
pinB=1  #GPIO1 è pin 12

Core::pin_mode(pinA,INPUT)     # A
Core::pin_mode(pinB,INPUT)     # B

Core::pull_mode(pinA,PUD_UP)   # Setto le resistenze come PULL UP (perchè: descritto su Word)
Core::pull_mode(pinB,PUD_UP) 

#Reset dei canali
new_value=0   
old_value=0                    # Partirò sempre da una condizione in cui i due canali sono allineati
cont=0
dbug=0

loop do   #inizio un ciclo infinito...
    
##################################################################################
# => Leggo i pin

A=Core::digital_read(pinA)
puts "il pin A è #{A}"            #A  Restituisce 1(alto) oppure 0(basso)
B=Core::digital_read(pinB)      #B
puts "il pin A è #{B}"          

################################## LOGICA ########################################
# =>  SEQUENZA    A   B   A^B   VALUE (4A +2B+A^B)
# =>  0           0   0   0     0     Non cambia 
# =>  1           1   0   1     3     1 giro ORARIO       B è in ritardo su A
# =>  2           1   1   0     5     2 giri ORARI or ANTIORARI -> da valutare!
# =>  3           0   1   1     4     1 giro ANTIORARIO   B è in anticipo su A

#NB: Se B è in anticipo ho moto ANTIORARIO, altrimenti ORARIO.
##################################################################################

# =>  Determino la direzione di rotazione ####

#1] XOR: A^B
xor=(A^B)

#2] Determino lo stato descritto nell'ultima tabella "VALUE"
new_value = ( A*2 + B*3 + xor*1 )   # Moltiplico A per 2 e B per 3 per produrre VALUE 
                                     # mentre xor sarà sempre 1 o 0
#3] Valuto lo stato 

if new_value == 3 then
  puts "1 giro  ORARIO"
  cont = cont + 1
  
elsif new_value == 5 then
    puts "2 giri: ORARI OR ANTIORARI ??? Devo valutare..."  #Ho due casi: se prima ero in ORARIO allora avrò 2x ORARI e viceversa
    
    if old_value == 3 then 
      cont = cont + 2 
      puts "2x ORARIO"       # prima ero ORARIO, allora mi sto muovendo x2 ORARIO
    elsif old_value == 4 then
      cont = cont - 2
      puts "2x ANTIorario"  # prima ero ANTIORARIO, allora mi sto muovendo x2 ANTIorario
    else puts "condizione non soddisfatta"          # uso dbug perchè sono OBBLIGATO a utilizzare l'else nel formato < if..then..elsif..else..end >
    end
  
elsif new_value == 4 then
      puts "1 giro ANTIorario"
      cont = cont - 1
else # è 0
      puts "Non cambia"
      cont = cont + 0
      
end

puts cont
      
#dbug = 0                # Potrei ritrovarmi un valore dbug elevatissimo e magari avere problemi. Boh! Resettiamolo...
old_value = new_value   # Update dell'ultimo valore letto

    


end #il programma finisce quando stacco l'alimentazione al Raspberry
                                    

