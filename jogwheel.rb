################################################
###  jogwheel.rb
###  Questo programma permette la movimentazione manuale del manipolatore cartesiano Bosch-Rexroth.
###  Istruzioni telecomando:
###  1] Una volta acceso, il programma parte automaticamente (DA FARE)
###  2] Di default la macchina rimane ferma nonstante si muova la rotellina. Per poter muovere un asse è necessario 
###     selezionare il relativo bottone.
###  3] Una volta scelto, utilizzare la manovella per determinare lo spostamento. Ripremere il bottone per confermare e uscire.
###     NB: non è possibile selezionare un altro asse senza aver ripremuto l'asse corrente.
###  4] È possibile determinare in qualsiasi momento lo step di precisione (in mm) con l'apposito bottone.
###
###  Alessandro Grillo, 8 Novembre 2015.
################################################


### QUESTO PROGRAMMA È SOGGETTO A BUG :
### •1 CLICK NON è SEMPRE UNO SPOSTAMENTO
### •AD UN CERTO PUNTO ESCE!!!

begin
  
  
include Raspberry
include Raspberry::Core
include Raspberry::Timing
include Raspberry::Specifics

#### Impostazione pin per pulsanti ####
#Encoder
$pinA=0   # A channel
$pinB=1   # B channel
$butX=4   # Bottone X 
$butY=5   # Bottone Y
$butZ=6   # Bottone Z
$butS=2   # Bottone step (risoluzione)

#### Impostazione pin in INPUT / OUTPUT e resistenze ####

#Encoder
Core::pin_mode($pinA,INPUT) 
Core::pin_mode($pinB,INPUT)     
Core::pull_mode($pinA,PUD_UP)
Core::pull_mode($pinB,PUD_UP)
#Bottoni
Core::pin_mode($butX,INPUT) 
Core::pin_mode($butY,INPUT) 
Core::pin_mode($butZ,INPUT) 
Core::pin_mode($butS,INPUT) 
Core::pull_mode($butX,PUD_UP) 
Core::pull_mode($butY,PUD_UP) 
Core::pull_mode($butZ,PUD_UP) 
Core::pull_mode($butS,PUD_UP) 



    
class Machine
  
  @ENABLE_TIME=500e3.to_i # [ms]
    
  attr_accessor :x
  attr_accessor :y
  attr_accessor :z
  attr_accessor :selected
  
  def initialize()
    #Creazione degli oggetti: creo i 3 assi.
    @x= EasyIndra::Drive.new()
    @y= EasyIndra::Drive.new()
    @z= EasyIndra::Drive.new()
    
    # Eseguo un drive.init : dichiaro l'indirizzo di ogni asse
    @x.init("192.168.0.101", :position)
    @y.init("192.168.0.102", :position)
    @z.init("192.168.0.103", :position)
    
    # Inizializzo la libreria easyindra
    puts "- Inizializzo Easyindra -"
    EasyIndra::init_easyindra([@x, @y, @z])
    
    # Procedura di reset, stacco, riconnessione
    puts "- Reset status -"
    @x.connect()
    @y.connect()
    @z.connect()
    EasyIndra::reset_status()
    puts "done"
    @x.disconnect()
    @y.disconnect()
    @z.disconnect()
    puts "- Connetto i drives -"
    @x.connect()
    @y.connect()
    @z.connect()
    puts "done"
    puts ""
  end
  
  ## Attivazione drives: ora posso comunicare con la macchina.
  def enable()
    
    puts "- Enable drives -"
    @x.enable()
    @y.enable()
    @z.enable()
    usleep(@ENABLE_TIME)
    puts "OK"
    puts ""
    
    position #chiamo la procedura sotto chiamata "pos" e rilevo la posizione della macchina all'avvio
    
  end
  
  ## Disabilitazione macchina.
  def disable()
    puts "- Disable drives -"
    @x.disable()
    @y.disable()
    @z.disable()
    puts "OK"
    puts "- Disconnessione drives -"
    @x.disconnect()
    @y.disconnect()
    @z.disconnect()
    puts "OK"
    puts ""
    puts "Exit EasyIndra library..."
    EasyIndra::shutdown()
    puts "OK"
    puts ""
  end
  
  #posizione attuale
  def position()
     @coords=[@x.get_position, @y.get_position, @z.get_position]
     puts @coords
     return @coords
  end
  
  def newpos(n)
    
    case @selected      
      when "x"
        @coords[0]+=(n)
        @x.setpoint(@coords[0])       
      when "y"
        @coords[1]+=(n)
        @y.setpoint(@coords[1])       
      when "z"
        @coords[2]+=(n)
        @z.setpoint(@coords[2])
      when 0
        #non fare nulla
      else
    end
    
  end

 
end






class Tools
  
  attr_accessor :step
  attr_accessor :old_value
  attr_accessor :new_value
  attr_accessor :cont
  
  def initialize()
    @step=1               #di default 1 click= 1 step = 1mm
    @old_value=1
    @new_value=0
    @cont=0  
    
    @stateX=1
    @stateY=1
    @stateZ=1
    @axe=0
    
    @stateS=1
    @i=0

    @ary=[1.0, 0.1]
    # OLD: NON FUNZIONA CON 0.01 E 0.001     @ary=[1.0, 0.1, 0.01, 0.001]
    @s=1
         
  end    
  
  def rotary()    
    
    A=Core::digital_read($pinA)
    B=Core::digital_read($pinB)

    xor=(A^B)

    #Sono al 1° avvio: sicomme l'encoder è fermo, per solo il caso in cui old_value=1  definito nelle costanti
    # • Metto old_value = new_value
    # • Mi esegue il codice solo 1 volta
    # • Prosegue e non mi fa i confronti, così restituisce un valore 0: sono fermo.

    if @old_value==1 then
      @old_value = ( A*2 + B*3 + xor*1 ) #mi cambia subito il valore da 1 a (0,3,5 or 4) dunque verrà eseguito SOLO all'avvio.
    else 
    end

    @new_value = ( A*2 + B*3 + xor*1 )

    if @new_value!=@old_value then

      if @new_value==0 then
        if @old_value==4 then
          @cont=1
        elsif @old_value==3
          @cont=(-1) 
        else
        end
      end

      if @new_value==3 then
        if @old_value==0 then
          @cont=1          
        elsif @old_value==5
          then 
          @cont=(-1)   
        else
        end
      end

      if @new_value==5 then
        if @old_value==3 then
          @cont=1
        elsif @old_value==4
          then 
          @cont=(-1)   
        else
        end
      end

      if @new_value==4 then
        if @old_value==5 then
          @cont=1
        elsif @old_value==0
          then 
          @cont=(-1)   
        else
        end
      end

    else
      @cont=0
    end

    @old_value = @new_value
    
    return @cont 
      
    #return NON E' necessario, ma è buona cosa rendere chiaro cosa mi ritorna il blocco di programma per una migliore lettura.
    
  end
  
  def axe_button()
    
    Xput=Core::digital_read($butX)             #Bottone premuto= 0,  altrimenti 1
    Yput=Core::digital_read($butY)
    Zput=Core::digital_read($butZ)
  
    # X button
    if Xput==0 && @stateX==1 then             #Premo il bottone: da 1->0
      puts "1) Confermo X"
      @stateX=0                               #Update stato: è premuto
      @axe="x"
      Timing::delay(200)

    elsif Xput==1 && @stateX==0 then          #Rilascio il bottone: se prima era premuto then...finchè non lo ripremo.
   
      @axe="x"   
      @stateY=2                               #Disabilito altri pulsanti
      @stateZ=2
      
      return @axe
      
    elsif Xput==0 && @stateX==0 then          #Ripremo il bottone per confermare.
    
      puts "3) RICONFERMO X"
      @stateX=1
  
      @stateY=1                               #Riattivo altri pulsanti
      @stateZ=1
      @axe=0                                  #Non sto selezionando nulla.
      Timing::delay(500)
      
    else
    end

    # Y button
    if Yput==0 && @stateY==1 then
      puts "1) Confermo Y"
      @stateY=0
      @axe="y"
      Timing::delay(200)

    elsif Yput==1 && @stateY==0 then
      @axe="y"
      @stateX=2  
      @stateZ=2
      
      return @axe
    
    elsif Yput==0 && @stateY==0 then
      puts "3) RICONFERMO Y"
      @stateY=1
  
      @stateX=1  
      @stateZ=1
      @axe=0
      Timing::delay(500)
      
    else
    end
  
    # Z button
    if Zput==0 && @stateZ==1 then
      puts "1) Confermo Z"
      @stateZ=0
      @axe="z"
      Timing::delay(200)

    elsif Zput==1 && @stateZ==0 then
      @axe="z"
      @stateX=2  
      @stateY=2
      
      return @axe
    
    elsif Zput==0 && @stateZ==0 then
      puts "3) RICONFERMO Z"
      @stateZ=1
  
      @stateX=1   
      @stateY=1
      @axe=0
      Timing::delay(500)
      
    else
    end
    
  end
  
  def axe_step()
    
    $Sput=Core::digital_read($butS)           #Bottone premuto= 0, altrimenti 1
  
    @i=0 if @i==2                             #Permette di ritornare all'inizio dell'array una volta superato i=2

    if $Sput==0 && @stateS==1 then            #Premo il bottone: da 1->0
   
      @stateS=0                               #Se premuto, aggiorna lo stato di servizio come 0 (premuto)
      @i+=1                                   #Incrementa contatore di 1
                
      @s=@ary[@i]                             #Leggo la posizione i-esima dell'array
      puts "#{@s}"                            #Scrivo la risoluzione scelta
      
      
      Timing::delay(200)
    
      

    elsif $Sput==0 && @stateS==0 then         #Ripremo il bottone per cambiare.
    
      @stateS=1
      @i+=1
      
      if @i>=2 then                           #Riporto il contatore all'inizio se con il contatore supero la dimensione
        @i=@i-2
      end
      
      @s=@ary[@i]
      puts "#{@s}"      
      
      Timing::delay(200)
    
    else
      return @s                               #Ritorna il valore scelto prima
    end
    
    return @s
    
  end
    
    
  
end





class Main
  
  attr_accessor :assi         
  attr_accessor :encoder      
  
  def initialize()
    @assi=Machine.new()        # Creo un nuovo oggetto "assi" contenente i tre assi.
    @encoder=Tools.new()       # Creo un nuovo oggetto "encoder".
    @laststep=@encoder.step
    @tot=0  
    @add=0
  end
  
  def resolution()
    
    @encoder.rotary()                           # Determino @cont,  valore +1 (orario), 0, -1 (antiorario) 
    
    @tot+=@encoder.cont                         # Parte da 0: aggiungo il valore cui sopra.
    
    if @laststep==@encoder.step then            #Caso particolare–Esempio: quando cambio step (1 a 0.1) e cont=1(old)+0.1(new) = 1.1 
                                                #non rispetta alcuna condizione cui sopra. Imposto tot=0 per ripartire con conteggio corretto.
      if @tot==4 then
        #puts "-->> a { DX }"
        @assi.newpos(@encoder.step)
        @tot=0    
      elsif @tot==(-4) then
        #puts "<<-- a { SX }"
        @assi.newpos((-1)*@encoder.step)
        @tot=0
      else      
      end
      
    else
      @tot=0
    end
        
    @laststep=@encoder.step                    #Update dell'ultimo step selezionato.
  end           
   

  def set_step()                               
    @encoder.step=@encoder.axe_step()                         
  end
  
  def set_assi(n)
    @assi.selected=n
  end
  
  def set_button()
    @encoder.axe_button() 
  end

end
    
    
################################################
### =>  PARTE LOGICA: 2 Novembre 2015
### =>  Alessandro Grillo
################################################

controller=Main.new()               #Crea:   @assi=Machine.new()  &    @encoder==Tools.new()  

controller.assi.enable()            # Abilito i drives: comunico con la macchina
controller.assi.position()          # Ottengo la posizione come array

# Ciclo di lettura e setting posizione

loop do
  
  controller.set_step()                           # Lettura STEP (da bottone): #encoder.step=(0.1) - #encoder.step=(0.01) - #encoder.step=(0.001)
  
  controller.set_assi( controller.set_button() )  # Lettura ASSE (da bottone)
  
  controller.resolution()                         # Nuovo posizionamento
  
  
end

rescue
  puts "Qualcosa non va!"
end