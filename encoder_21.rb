#################################################################################
#      Questo sketch permette la lettura di un encoder ottico incrementale
#      dotato di 2 canali A,B.
#      Alessandro Grillo, 12/10/2015 (È il mio compleanno!)
#################################################################################

include Raspberry
include Raspberry::Core
include Raspberry::Timing
include Raspberry::Specifics

 
pinA=0  #GPIO0 è pin 11   # Assegno il numero di pin per rendere la struttura facilmente modificabile
pinB=1  #GPIO1 è pin 12

Core::pin_mode(pinA,INPUT)     # A
Core::pin_mode(pinB,INPUT)     # B

Core::pull_mode(pinA,PUD_UP)   # Setto le resistenze come PULL UP (perchè: descritto su Word)
Core::pull_mode(pinB,PUD_UP) 


#Reset dei canali
new_value=0   
old_value=1                    # Partirò sempre da una condizione in cui i due canali sono allineati
cont=0
tot=0

loop do

# => Leggo i pin: Restituisce 1(alto) oppure 0(basso)
A=Core::digital_read(pinA)
B=Core::digital_read(pinB)
puts "A è #{A}"
puts "B è #{B}"
        
################################## LOGICA ########################################
# =>  SEQUENZA    A   B   A^B   VALUE (4A +2B+A^B)
# =>  0           0   0   0     0     Non cambia 
# =>  1           1   0   1     3     1 giro ORARIO       B è in ritardo su A
# =>  2           1   1   0     5     2° giro ORARIO or ANTIORARIO -> da valutare!
# =>  3           0   1   1     4     1 giro ANTIORARIO   B è in anticipo su A
##################################################################################

#1] XOR: A^B
xor=(A^B)

#Primissimo avvio!

if old_value==1 then
  old_value = ( A*2 + B*3 + xor*1 )
else 
  next
end

#2] Determino lo stato descritto nell'ultima tabella "VALUE"
new_value = ( A*2 + B*3 + xor*1 )   # Moltiplico A per 2 e B per 3 per produrre VALUE, xor sarà sempre 1 o 0
              
#3] Valuto lo stato 
if new_value!=old_value then

  if new_value == 3 then
  puts "1 giro  ORARIO"
  cont = 1

  elsif new_value == 5 then
    
    puts "2 giri: ORARI OR ANTIORARI ??? Devo valutare..."  #Ho due casi: se prima ero in ORARIO allora avrò 2x ORARI e viceversa
    
    if old_value == 3 then
      cont = 1 
      puts "2° ORARIO"          # prima ero ORARIO, allora mi sto muovendo con un 2° giro ORARIO
    elsif old_value == 4 then   # NON metto else perchè magari ho un errore allora, se è 4 OK, altrimenti va avanti e rinizia la lettura (next)
      cont = -1
      puts "2° ANTIorario"  # prima ero ANTIORARIO, allora mi sto muovendo per un 2° giro ANTIorario
    else
      next
    end
  
  elsif new_value == 4 then
      puts "1 giro ANTIorario"
      cont =  -1
      
  elsif new_value == 0 then
      puts "Sei sul canale nullo"
      cont = 0
  else
    next
  end
  
else
  next
end
      
    
Timing::delay(500)

tot+=cont

puts "Contatore: #{cont}"
puts "Totale: #{tot}"


cont=0   
old_value = new_value   # Update dell'ultimo valore letto

end