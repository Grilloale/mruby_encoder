
include Raspberry
include Raspberry::Core
include Raspberry::Timing
include Raspberry::Specifics




ENABLE_TIME   = 500e3.to_i # [ms]
SETTLING_TIME = 3e6.to_i   # [ms]

puts "Start"
puts 

############INIZIALIZZAZIONE MACCHINA################

puts "1] create 3 axes ..."
driveX = EasyIndra::Drive.new()
driveY = EasyIndra::Drive.new()
driveZ = EasyIndra::Drive.new()
puts "done"

puts "2] set parameters (ip, control_mode) ..."
driveX.init("192.168.0.101", :position)
driveY.init("192.168.0.102", :position)
driveZ.init("192.168.0.103", :position)
puts "done"

puts "3] initialize easyindra ..."
EasyIndra::init_easyindra([driveX, driveY, driveZ])
puts "done"


puts "4] reset status:ATTACCO - STACCO - ATTACCO"
driveX.connect()
driveY.connect()
driveZ.connect()
EasyIndra::reset_status()
driveX.disconnect()
driveY.disconnect()
driveZ.disconnect()
puts "connect drives ..."
driveX.connect()
driveY.connect()
driveZ.connect()
puts "done"

puts "5] enable drives ..."
driveX.enable()
driveY.enable()
driveZ.enable()
usleep(ENABLE_TIME)
puts "done"

############################################################


driveX.setpoint(200.000)
driveY.setpoint(100.000)
driveZ.setpoint(-5.000)
usleep(SETTLING_TIME)
puts "Fatto"

############################################################

puts "disable drives ..."
driveX.disable()
driveY.disable()
driveZ.disable()
puts "done"

puts "disconnect drives ..."
driveX.disconnect()
driveY.disconnect()
driveZ.disconnect()
puts "done"

puts "exit EasyIndra library..."
EasyIndra::shutdown()
puts "done"

puts "Done!"