################################################
### =>  Oggi è il: 30 Ottobre 2015
### =>  Alessandro Grillo
################################################

include Raspberry
include Raspberry::Core
include Raspberry::Timing
include Raspberry::Specifics

#### Impostazione pin per pulsanti ####
#Encoder
$pinA=0   # A channel
$pinB=1   # B channel

#### Impostazione pin in INPUT / OUTPUT ####
Core::pin_mode($pinA,INPUT) 
Core::pin_mode($pinB,INPUT)     

#@ variabile di istanza
#@@ variabile di classe
# $ variabile globale
    
class Machine
  
  @ENABLE_TIME=500e3.to_i # [ms]
    
  attr_accessor :x
  attr_accessor :y
  attr_accessor :z
  attr_accessor :selected
        
  #posso leggere o scrivere sulla variabile di classe @step 
  #da fuori chiamo: machine.step=valore da settare 
  #quindi 1/10/100 mm
  
  ### Inizializzazione oggetto: quando è creato, connetto la macchina.
  def initialize()
    #Creazione degli oggetti: creo i 3 assi.
    @x= EasyIndra::Drive.new()
    @y= EasyIndra::Drive.new()
    @z= EasyIndra::Drive.new()
    
    # Eseguo un drive.init : dichiaro l'indirizzo di ogni asse
    @x.init("192.168.0.101", :position)
    @y.init("192.168.0.102", :position)
    @z.init("192.168.0.103", :position)
    
    # Inizializzo la libreria easyindra
    puts "- Inizializzo Easyindra -"
    EasyIndra::init_easyindra([@x, @y, @z])
    
    # Procedura di reset, stacco, riconnessione
    puts "- Reset status -"
    @x.connect()
    @y.connect()
    @z.connect()
    EasyIndra::reset_status()
    puts "done"
    @x.disconnect()
    @y.disconnect()
    @z.disconnect()
    puts "- Connetto i drives -"
    @x.connect()
    @y.connect()
    @z.connect()
    puts "done"
    puts ""
  end
  
  ## Attivazione drives: ora posso comunicare con la macchina.
  def enable()
    
    puts "- Enable drives -"
    @x.enable()
    @y.enable()
    @z.enable()
    usleep(@ENABLE_TIME)
    puts "done"
    puts ""
    
    position #chiamo la procedura sotto chiamata "pos" e rilevo la posizione della macchina all'avvio
    
  end
  
  ## Disabilitazione macchina.
  def disable()
    puts "- Disable drives -"
    @x.disable()
    @y.disable()
    @z.disable()
    puts "done"
    puts "- Disconnessione drives -"
    @x.disconnect()
    @y.disconnect()
    @z.disconnect()
    puts "done"
    puts ""
    puts "Exit EasyIndra library..."
    EasyIndra::shutdown()
    puts "done"
    puts ""
  end
  
  #posizione attuale
  def position()
     @coords=[@x.get_position, @y.get_position, @z.get_position]
     puts @coords
     return @coords
  end
  
  def newpos(n)
    
    case @selected 
      #gli passo il simbolo determinato, es. :x
      
      when "x"
        @coords[0]+=(n)
        @x.setpoint(@coords[0])
        
        #tot=0
       
      when "y"
        @coords[1]+=(n)
        @y.setpoint(@coords[1])
       
        #tot=0
        
      when "z"
        @coords[2]+=(n)
        @z.setpoint(@coords[2])
        
        #tot=0
      else
        #db+=0
    end
  end
  
end

class Tools
  
  attr_accessor :step
  attr_accessor :old_value
  attr_accessor :new_value
  attr_accessor :cont
  
  def initialize()
    @step=1               #di default giro e ho 1 step = 1mm
    @old_value=1
    @new_value=0
    @cont=0       
  end    
  
  def rotary()    
    # Leggo i pin: Restituisce 1(alto) oppure 0(basso)
    A=Core::digital_read($pinA)
    B=Core::digital_read($pinB)

    xor=(A^B)

    #Sono al 1° avvio: sicomme l'encoder è fermo, per solo il caso in cui old_value=1  definito nelle costanti
    # • Metto old_value = new_value
    # • Mi esegue il codice solo 1 volta
    # • Prosegue e non mi fa i confronti, così restituisce un valore 0: sono fermo.

    if @old_value==1 then
      @old_value = ( A*2 + B*3 + xor*1 ) #mi cambia subito il valore da 1 a (0,3,5 or 4) dunque verrà eseguito SOLO all'avvio.
    else 
    end

    @new_value = ( A*2 + B*3 + xor*1 )

    if @new_value!=@old_value then

      #NB: DEVO RIMETTERE: @cont=@step*1

      if @new_value==0 then
        if @old_value==4 then
          @cont=@step*1
        elsif @old_value==3
          @cont=@step*(-1) 
        else
        end
      end

      if @new_value==3 then
        if @old_value==0 then
          @cont=@step*1          
        elsif @old_value==5
          then 
          @cont=@step*(-1)   
        else
        end
      end

      if @new_value==5 then
        if @old_value==3 then
          @cont=@step*1
        elsif @old_value==4
          then 
          @cont=@step*(-1)   
        else
        end
      end

      if @new_value==4 then
        if @old_value==5 then
          @cont=@step*1
        elsif @old_value==0
          then 
          @cont=@step*(-1)   
        else
        end
      end

    else
      @cont=0
    end

    @old_value = @new_value
    
    return @cont 
      
    #return NON E' necessario, ma è buona cosa rendere chiaro cosa mi ritorna il blocco di programma per una migliore lettura.
    #NB nel programma principale dovrò fare il ciclo con : se sono =4 allora 1 giro..
    
  end
  
end

class Main
  
  attr_accessor :assi         
  attr_accessor :encoder      
  
  def initialize()
    @assi=Machine.new()        # Creo un nuovo oggetto "assi" contenente i tre assi.
    @encoder=Tools.new()       # Creo un nuovo oggetto "encoder".
    @laststep=@encoder.step
    @tot=0  
  end
  
  def resolution()
    
    @encoder.rotary()                           # Determino @cont,  valore +1 (orario), 0, -1 (antiorario) 
    
    @tot+=@encoder.cont                          # Parte da 0: aggiungo il valore cui sopra.
    
    if @laststep==@encoder.step then
      if @tot==@encoder.step*4 then
        puts "-->> a { DX }"
        @assi.newpos(@encoder.step)
        @tot=0    
      elsif @tot==@encoder.step*(-4) then
        puts "<<-- a { SX }"
        @assi.newpos(@encoder.step)
        @tot=0
      else     
      end
    else
      @tot=0
    end
    
    @laststep=@encoder.step 
  end
                 
   
   # HO TOLTO IL CASO PARTICOLARE 
   #
    # else
     # @tot=0
      #puts @laststep                                 #Caso particolare: quando cambio step (1 a 0.1) e cont=1(old)+0.1(new) = 1.1 
      #end                                        #non rispetta alcuna condizione cui sopra. Imposto tot=0 per ripartire con conteggio corretto
                  
   
  def set_step(n)
    @encoder.step=n                            #se la prende dal bottone
  end
  
  def set_assi(n)
    @assi.selected=n
  end

end
    
    
################################################
### =>  PARTE LOGICA: 30 Ottobre 2015
### =>  Alessandro Grillo
################################################

controller=Main.new()               #Crea:   @assi=Machine.new()  &    @encoder==Tools.new()  

controller.assi.enable()            # Abilito i drives: comunico con la macchina
controller.assi.position()          # Ottengo la posizione come array

# Ciclo di lettura e setting posizione

loop do
  
  controller.set_step(1.0)             # Lettura STEP (da bottone): #encoder.step=(0.1)   #encoder.step=(0.01)    #encoder.step=(0.001)
  controller.set_assi("x")           # Lettura ASSE (da bottone)
  
  controller.resolution()
  
end


  




 
  
  
      
      
  
  
  
      
      
      
      
      
      
     